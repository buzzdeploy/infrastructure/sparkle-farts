#!/bin/bash
# Used for pulling kubeconfig stored in the Linode Bucket: Needs python container

# Set variables from terraform state
uniqueid="s"$(grep "unique_number" main.tf |awk -F'\"' '{print $2}')
configname=${uniqueid}"config"
echo ${configname}

# Source to get variable beyond script scope
export KUBECONFIG="$(pwd)"/${configname}

# Pull kubeconfig from an S3 bucket and if success, output name of kubeconfigfile
linode-cli obj get k3sfiles ${configname} --cluster us-east-1

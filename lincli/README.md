# Linode CLI

[[_TOC_]]

Just to have it in here, for the Linode-CLI, the commands and notes are in this
folder for replication for other parties. No use showing off anything fun if 
people need to got hunting for the same information!

## Setup

1. Run `virtualenv .` in this folder. 
1. Run `source bin/activate`
1. Set up the environment with `pip install -r requirements.txt`
1. Go login into the Linode [console](https://cloud.linode.com/dashboard)
1. Click on `API Tokens` and create add a personal token. Save token value some place immediately. 
1. On the command line, run `linode-cli configure` and paste in the token when prompted.

*Note:* VirtualEnv is not needed for use in pipelines, any requirements file for local use only.

# Ansible Inventory Generation

After Terraform is run, the respective names and groups can be pulled from
linode and turned into an Ansible inventory in terms of host name and ipv4 
address. The example is shown below. 

```
$ linode-cli linodes list --json > listjson.json
$ python invcreate.py listjson.json 

[masters]
k3smaster1 ansible_host=45.79.128.159

[workers]
k3sworker0 ansible_host=104.200.31.150
k3sworker1 ansible_host=172.104.5.169
```


# Generated Module Variables
